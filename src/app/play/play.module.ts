import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayRoutingModule } from './play-routing.module';
import { MovieComponent } from './movie/movie.component';
import { WatchComponent } from './watch/watch.component';


@NgModule({
  declarations: [MovieComponent, WatchComponent],
  imports: [
    CommonModule,
    PlayRoutingModule
  ]
})
export class PlayModule { }
