import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Movie } from '../../shared/movie';
import { OmdbService } from '../../shared/omdb.service';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.scss']
})
export class WatchComponent implements OnInit {

  movie: Movie;

  constructor(
    private readonly omdb: OmdbService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params
      .pipe(
        mergeMap((params) => {
          return this.omdb.get(params.id);
        })
      )
      .subscribe((movie) => {
        this.movie = movie;
      });
  }

}
