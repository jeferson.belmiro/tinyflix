import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from '../../dialog/dialog.service';
import { State, StateService } from '../../shared/state.service';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  users: string;

  constructor(
    private readonly router: Router,
    private readonly user: UserService,
    private readonly state: StateService,
    private readonly dialog: DialogService,
  ) { }

  ngOnInit() {
    this.state.get().pipe(first()).subscribe(
      (state: State) => {
        const data = state.users.map((user) => {
          return {
            id: user.id,
            username: user.username,
            password: user.password,
            movies: user.movies.length,
          };
        });
        this.users = JSON.stringify(data, null, 2);
      }
    );
  }

  signIn() {

    if (!this.form.valid) {
      return;
    }

    this.user.login(this.form.get('username').value, this.form.get('password').value).subscribe(
      () => {
        this.router.navigate(['/']);
      },
      (err) => {
        console.error('signIn', err);
        this.dialog.alert('Ops...', err.message);
      },
    );
  }

}
