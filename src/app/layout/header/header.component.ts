import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user;

  constructor(
    private readonly userService: UserService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.userService.get().subscribe(response => this.user = response);
  }

  logout() {
    this.userService.logout();
  }

  profile() {
    this.router.navigate(['/profile', this.user.id]);
  }

}
