import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  title: string;
  message: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    const { title, message } = this.data;
    this.title = title;
    this.message = message;
    if (!message) {
      this.title = '';
      this.message = title;
    }
  }

  ngOnInit() {
  }

}
