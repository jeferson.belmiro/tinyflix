import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from './alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  alert(title: string, message?: string) {
    return this.dialog.open(AlertComponent, {
      width: '400px',
      data: { title, message },
    });
  }
}
