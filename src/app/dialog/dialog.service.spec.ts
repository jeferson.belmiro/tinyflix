import { TestBed } from '@angular/core/testing';
import { DialogModule } from './dialog.module';
import { DialogService } from './dialog.service';

describe('DialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      DialogModule,
    ]
  }));

  it('should be created', () => {
    const service: DialogService = TestBed.get(DialogService);
    expect(service).toBeTruthy();
  });
});
