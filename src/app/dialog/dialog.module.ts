import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { AlertComponent } from './alert/alert.component';
import { DialogService } from './dialog.service';

@NgModule({
  declarations: [
    AlertComponent,
  ],
  entryComponents: [
    AlertComponent,
  ],
  providers: [
    DialogService,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ]
})
export class DialogModule { }
