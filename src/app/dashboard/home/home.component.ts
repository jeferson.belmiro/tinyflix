import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { delay, finalize, switchMap } from 'rxjs/operators';
import { LoaderService } from '../../loader/loader.service';
import { MovieService } from '../../shared/movie.service';
import { OmdbService } from '../../shared/omdb.service';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // @TODO - add Movie interface
  sections = [];

  constructor(
    private readonly omdb: OmdbService,
    private readonly loader: LoaderService,
    private readonly user: UserService,
    private readonly movie: MovieService,
  ) { }

  ngOnInit() {

    this.loader.show();

    // shame!!!
    let userCountry: string;

    this.user.get().pipe(
      switchMap((user) => {

        userCountry = user.country;
        const streams = [
          this.movie.getTopFromCountry(user.country),
          this.movie.getLastWatchFromUser(user.id),
          this.user.getTopWatchers(),
          this.movie.getTopRated(),
          this.movie.getTopFromGenre('Action'),
          this.movie.getTopFromGenre('Fantasy'),
        ];
        return forkJoin(streams);
      }),
      finalize(() => this.loader.hide()),
    )
    .subscribe(
      ([ country, user, users, rated, action, fantasy ]: any) => {

        this.sections = [
          { label: 'Top watchers', users, },
          { label: `Top from ${userCountry}`, movies: country, },
          { label: 'You last watched', movies: user, },
          { label: 'Top rated', movies: rated, },
          { label: 'Action', movies: action, },
          { label: 'Fantasy', movies: fantasy, },
        ];
      },
    );

  }

  loadMovies(ids: string[]) {
    return ids.map((id: string) => {
      // const time  = Math.ceil(Math.random() * 10) * 1000;
      return this.omdb.get(id).pipe(delay(0));
    });
  }

}
