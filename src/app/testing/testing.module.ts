import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { first } from 'rxjs/operators';
import { State, StateService } from '../shared/state.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NoopAnimationsModule,
    HttpClientTestingModule,
    RouterTestingModule,
  ],
})
export class TestingModule {

  constructor(private readonly stateService: StateService) {
    this.stateService.get().pipe(first()).subscribe(
      (state: State) => {
        state.user = state.users.find((user: any) => user.id === 1);
        this.stateService.set(state);
      }
    );
  }

}
