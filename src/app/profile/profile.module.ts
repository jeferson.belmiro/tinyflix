import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { SectionModule } from '../section/section.module';
import { MainComponent } from './main/main.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { AddDialogComponent } from './add-dialog/add-dialog.component';

@NgModule({
  declarations: [
    MainComponent,
    AddDialogComponent
  ],
  entryComponents: [
    AddDialogComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    ReactiveFormsModule,
    SectionModule,
    MatListModule,
    MatIconModule,
    MatRippleModule,
    MatDialogModule,
  ]
})
export class ProfileModule { }
