import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../../loader/loader.service';
import { OmdbService } from '../../shared/omdb.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-dialog',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss']
})
export class AddDialogComponent implements OnInit {

  imdbID = new FormControl('', Validators.required);
  errorMessage: string;

  constructor(
    private readonly omdb: OmdbService,
    private readonly loader: LoaderService,
    private readonly dialogRef: MatDialogRef<AddDialogComponent>,
  ) { }

  ngOnInit() {
  }

  add() {

    if (!this.imdbID.value) {
      return;
    }

    const defaultErrorMessage = 'Incorrect IMDb ID.';

    this.loader.show();
    this.errorMessage = '';

    this.omdb.get(this.imdbID.value)
    .pipe(
      finalize(() => this.loader.hide())
    )
    .subscribe(
      (response: any) => {
        if (!response) {
          this.errorMessage = defaultErrorMessage;
          return;
        }
        if (response.Error) {
          this.errorMessage = response.Error;
          return;
        }
        this.dialogRef.close(response);
      },
      (err: any) => {
        console.error('add', err);
        this.errorMessage = err.message || defaultErrorMessage;
      },
    );
  }

}
