import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { first, map, mergeMap, filter } from 'rxjs/operators';
import { Movie } from '../../shared/movie';
import { MovieService } from '../../shared/movie.service';
import { State, StateService } from '../../shared/state.service';
import { UserService } from '../../shared/user.service';
import { AddDialogComponent } from '../add-dialog/add-dialog.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user: any;
  movies: Movie[] = [];
  isUserLogged: boolean;

  constructor(
    private readonly userService: UserService,
    private readonly movieService: MovieService,
    private readonly route: ActivatedRoute,
    private readonly dialog: MatDialog,
    private readonly state: StateService,
  ) { }

  ngOnInit() {
    this.route.params
      .pipe(
        filter((params) => params.id),
        mergeMap((params) => {

          this.userService.get().subscribe(user => {
            this.isUserLogged = user.id === +params.id;
          });
          const length = this.isUserLogged ? 0 : 5;
          return forkJoin([
            this.userService.getById(+params.id),
            this.movieService.getLastWatchFromUser(+params.id, length),
          ]);
        })
      )
      .subscribe(([user, movies]) => {
        this.user = user;
        this.movies = movies;
      });
  }

  add(): void {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((movie: Movie) => {
      if (movie) {
        this.updateState(movie);
      }
    });
  }

  updateState(movie: Movie) {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const user = state.users.find((item) => item.id === state.user.id);
        if (!user) {
          throw new Error('User not found');
        }
        const has = user.movies.find((item: any) => item.imdbID === movie.imdbID);
        if (has) {
          throw new Error('Duplicated movie');
        }

        this.movies.unshift(movie);
        user.movies.push({
          imdbID: movie.imdbID,
          date: new Date().toISOString(),
          times: 1,
        });
        return state;
      }),
    ).subscribe(
      (updated: State) => {
        this.state.setValue('users', updated.users);
        this.state.persist();
      },
      (err: any) => console.error('updateState', err),
    );
  }

}
