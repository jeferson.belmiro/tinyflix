import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';
import { LoaderService } from './loader/loader.service';
import { StateService } from './shared/state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':leave', [
        animate(380, style({opacity: 0}))
      ])
    ])
  ]
})
export class AppComponent {

  loading = false;
  boot = true;

  constructor(
    private readonly router: Router,
    private readonly loader: LoaderService,
    private readonly state: StateService,
  ) {
    this.state.load();
    this.initLoader();
    this.loadingOnRouteStart();

    // small delay for lazzy loading router module
    // and not freeze animation
    setTimeout(() => this.boot = false, 500);
  }

  initLoader() {
    this.loader.getSubject().subscribe((display: boolean) => {
      // trick for change after view init
      setTimeout(() => this.loading = display);
    });
  }

  /**
   * show loader on lazzy loading module
   */
  loadingOnRouteStart() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof RouteConfigLoadStart) {
        this.loader.show();
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loader.hide();
      }
    });
  }

}
