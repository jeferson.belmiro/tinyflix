import { TestBed } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { TestingModule } from '../testing/testing.module';
import { AuthGuard } from './auth.guard';
import { StateService } from './state.service';
import { of } from 'rxjs';

describe('AuthGuard', () => {

  let guard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard],
      imports: [
        TestingModule,
        RouterModule,
      ],
    });

    guard = TestBed.get(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('canActivateChild - valid user', () => {

    const router = TestBed.get(Router);
    spyOn(router, 'navigate');

    guard.canActivateChild().subscribe((result: boolean) => {
      expect(result).toBeTruthy();
    });

    expect(router.navigate).not.toHaveBeenCalled();
  });

  it('canActivateChild - invalid user', () => {

    const state = TestBed.get(StateService);
    const router = TestBed.get(Router);
    spyOn(router, 'navigate');
    spyOn(state, 'get').and.returnValues(of({user: null}));

    guard.canActivateChild().subscribe((result: boolean) => {
      expect(result).toBeFalsy();
    });
    expect(router.navigate).toHaveBeenCalled();
  });

});
