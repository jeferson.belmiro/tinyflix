import { TestBed } from '@angular/core/testing';
import { TestingModule } from '../testing/testing.module';
import { Movie } from './movie';
import { MovieService } from './movie.service';
import { OmdbService } from './omdb.service';
import { of } from 'rxjs';
import { StateService } from './state.service';

describe('MovieService', () => {

  let service: MovieService;

  let globalId = 0;
  const MOVIES = new Map();

  const moviesMockFactory = (properties: Partial<Movie>, length = 5) => {
    for (let index = 0; index < length; index++) {
      const base = { imdbID: ++globalId };
      MOVIES.set(base.imdbID, { ...base, ...properties });
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
      ],
      providers: [
        {
          provide: StateService,
          useValue: {
            set() {},
            get() {
              const movies = Array.from(MOVIES.values());
              return of({
                users: [{ id: 1, movies }],
              });
            },
          },
        },
        {
          provide: OmdbService,
          useValue: {
            get(id: string) {
              return of(MOVIES.get(id));
            },
          },
        }
      ],
    });

    service = TestBed.get(MovieService);
    MOVIES.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getTopRated', () => {

    moviesMockFactory({ imdbRating: '5' }, 3);
    moviesMockFactory({ imdbRating: '6' }, 1);
    moviesMockFactory({ imdbRating: '8.5' }, 1);

    const expected = ['8.5', '6', '5', '5', '5'];
    let result: string[];

    service.getTopRated().subscribe((response: Movie[]) => {
      result = response.map(item => item.imdbRating);
    });

    expect(result).toEqual(expected);

  });

  it('getTopFromCountry', () => {

    moviesMockFactory({ Country: 'USA',  imdbRating: '5' }, 1);
    moviesMockFactory({ Country: 'USA',  imdbRating: '3' }, 2);
    moviesMockFactory({ Country: 'USA',  imdbRating: '10' }, 1);
    moviesMockFactory({ Country: 'Brazil',  imdbRating: '5' }, 5);

    const expected = [ '10', '5' , '3', '3'];
    let result: string[];

    service.getTopFromCountry('USA').subscribe((response: Movie[]) => {
      result = response.map(item => item.imdbRating);
    });

    expect(result).toEqual(expected);

  });

  it('getTopFromGenre', () => {

    moviesMockFactory({ Genre: 'Fantasy',  imdbRating: '6.9' }, 2);
    moviesMockFactory({ Genre: 'Comedy',  imdbRating: '3.0' }, 1);
    moviesMockFactory({ Genre: 'Action',  imdbRating: '6.7' }, 2);
    moviesMockFactory({ Genre: 'Action',  imdbRating: '8.1' }, 1);

    const expected = [ '8.1', '6.7' , '6.7' ];
    let result: string[];

    service.getTopFromGenre('Action').subscribe((response: Movie[]) => {
      result = response.map(item => item.imdbRating);
    });

    expect(result).toEqual(expected);

  });

  it('getLastWatchFromUser', () => {

    moviesMockFactory({ imdbRating: '7.1' }, 10);
    moviesMockFactory({ imdbRating: '10' }, 1);
    moviesMockFactory({ imdbRating: '8.1' }, 1);
    moviesMockFactory({ imdbRating: '6.0' }, 1);
    moviesMockFactory({ imdbRating: '3.3' }, 1);

    const expected = [ '3.3', '6.0', '8.1', '10', '7.1' ];
    let result: string[];

    service.getLastWatchFromUser(1).subscribe((response: Movie[]) => {
      result = response.map(item => item.imdbRating);
    });

    expect(result).toEqual(expected);

  });

});
