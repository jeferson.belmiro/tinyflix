import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { USERS } from './data';

export interface State {
  user: any;
  users: any[];
}

export const INITIAL_FACTORY = () => {
  return {
    user: null,
    users: USERS.slice(),
  } as State;
};

export const PERSIST_KEY = 'tinyflix-state';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private readonly state = new BehaviorSubject<State>(INITIAL_FACTORY());
  public readonly state$ = this.state.asObservable();

  constructor() { }

  get() {
    return this.state$;
  }

  set(state: State) {
    this.state.next({...state});
  }

  setValue(key: keyof State, value: any) {
    const state = this.state.getValue();
    state[key] = value;
    this.set(state);
  }

  clear() {
    this.set(INITIAL_FACTORY());
  }

  persist() {
    const state = this.state.getValue();
    const stringify = JSON.stringify(state);
    localStorage.setItem(PERSIST_KEY, stringify);
  }

  load() {
    const stringify = localStorage.getItem(PERSIST_KEY);
    const state = JSON.parse(stringify) || INITIAL_FACTORY();
    this.set(state);
  }

}
