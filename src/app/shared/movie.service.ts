import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { forkJoin, Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { Movie } from './movie';
import { OmdbService } from './omdb.service';
import { State, StateService } from './state.service';

const flatten = (data: any[]) => _.flatten(data);

const unique = (data: any[], key = 'imdbID') => {
  const keys = data.map(item => item[key]);
  return data.filter((obj, pos) => {
    return keys.indexOf(obj[key]) === pos;
  });
};

const getAllMap = (omdb: OmdbService) => {
  return (ids: string[]) => {
    const streams = ids.map((imdbID) => omdb.get(imdbID));
    return forkJoin(streams);
  };
};

const filterByKeyMap = (key: string, value: string) => {
  return (movies: Movie[]) => {
    const getCountries = (movie: Movie) => {
      return String(movie[key]).toLowerCase().replace(/\s+/g, '').split(',');
    };
    return movies.filter((movie: Movie) => {
      return getCountries(movie).includes(value.toLowerCase());
    });

  };
};

const orderByRatingMap = () => {
  return (movies: Movie[]) => {
    return movies.sort((left: Movie, right: Movie) => {
      return Number(right.imdbRating) - Number(left.imdbRating);
    });
  };
};

const reverseMap = () => (movies: Movie[]) => movies.reverse();

const sliceMap = (length: number) => {
  return (movies: Movie[]) => {
    if (length <= 0) {
      return movies.slice();
    }
    return movies.slice(0, length);
  };
};

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(
    private readonly state: StateService,
    private readonly omdb: OmdbService,
  ) { }

  getTopRated(length = 5) {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const movies = flatten(state.users.map((user) => user.movies));
        return unique(movies).map(item => item.imdbID);
      }),
      switchMap(getAllMap(this.omdb)),
      map(orderByRatingMap()),
      map(sliceMap(length)),
    );
  }

  getTopFromCountry(country: string, length = 5): Observable<Movie[]> {
    return this.getTopFromKey('Country', country, length);
  }

  getTopFromGenre(genre: string, length = 5) {
    return this.getTopFromKey('Genre', genre, length);
  }

  getTopFromKey(key: string, value: string, length = 5) {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const movies = flatten(state.users.map((user) => user.movies));
        return unique(movies).map(item => item.imdbID);
      }),
      switchMap(getAllMap(this.omdb)),
      map(filterByKeyMap(key, value)),
      map(orderByRatingMap()),
      map(sliceMap(length)),
    );
  }

  getLastWatchFromUser(id: number, length = 5): Observable<Movie[]> {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const user = state.users.find((item) => item.id === id);
        return user.movies.map((item: any) => item.imdbID);
      }),
      switchMap(getAllMap(this.omdb)),
      map(reverseMap()),
      map(sliceMap(length)),
    );
  }

}
