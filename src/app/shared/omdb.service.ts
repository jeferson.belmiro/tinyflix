import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { Movie } from './movie';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OmdbService {

  private url = `https://www.omdbapi.com/`;
  private cache = new Map();

  constructor(
    private readonly http: HttpClient,
  ) { }

  public search(name: string) {
    // @TODO - cast response
    return this.build({ name });
  }

  public get(id: string): Observable<Movie> {
    // @TODO - cast response
    return this.build<Movie>({ id });
  }

  public build<T>(options: any): Observable<T> {

    let params = new HttpParams()
      .append('apikey', environment.omdb_api_key)
      .append('type', 'movie');

    if (options.id) {
      params = params.append('i', options.id);
    } else if (options.name) {
      params = params.append('s', options.name);
    }

    if (options.page) {
      params = params.append('page', String(options.page));
    }

    if (options.year) {
      params = params.append('y', String(options.year));
    }

    if (options.plot) {
      params = params.append('plot', options.plot);
    }

    const cacheId = params.toString();
    if (this.cache.has(cacheId)) {
      return of(this.cache.get(cacheId));
    }

    return this.http.get<T>(this.url, { params }).pipe(
      tap((response: T) => {
        this.cache.set(cacheId, response);
      }),
    );
  }

}
