import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { INITIAL_FACTORY, State, StateService } from './state.service';

describe('StateService', () => {

  let service: StateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get', () => {

    const get = service.get();
    expect(get instanceof Observable).toBeTruthy();

  });

  it('set', () => {

    const state = { user: { id: 99 }, users: [] } as State;
    service.set(state);

    let result: State;
    service.get().subscribe((response: State) => result = response);
    expect(result).toEqual(state);

  });

  it('setValue', () => {

    service.setValue('user', { id: 10 });
    service.setValue('users', []);

    let result: State;
    service.get().subscribe((response: State) => result = response);
    expect(result).toEqual({user: { id: 10 }, users: []});

  });

  it('setValue', () => {

    service.setValue('user', { id: 10 });
    service.clear();

    let result: State;
    service.get().subscribe((response: State) => result = response);
    expect(result).toEqual(INITIAL_FACTORY());

  });

  it('persist', () => {

    spyOn(JSON, 'stringify').and.callThrough();
    spyOn(localStorage, 'setItem').and.callThrough();

    service.persist();

    expect(localStorage.setItem).toHaveBeenCalled();
    expect(JSON.stringify).toHaveBeenCalled();

  });

  it('load', () => {

    spyOn(JSON, 'parse').and.callThrough();
    spyOn(localStorage, 'getItem').and.callThrough();

    service.load();

    expect(localStorage.getItem).toHaveBeenCalled();
    expect(JSON.parse).toHaveBeenCalled();

  });

});
