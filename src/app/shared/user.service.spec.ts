import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { TestingModule } from '../testing/testing.module';
import { UserService } from './user.service';

describe('UserService', () => {

  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
      ],
    });
    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get', () => {

    let result: any;
    service.get().subscribe((response: any) => result = response);
    expect(result).toBeTruthy();
    expect(result.id).toBe(1);
  });

  it('getById', () => {

    let result: any;
    service.getById(2).subscribe((response: any) => result = response);
    expect(result).toBeTruthy();
    expect(result.id).toBe(2);

  });

  it('getTopWatchers', () => {

    let result: number[];
    const expected = [3, 5, 1, 2, 4];
    service.getTopWatchers().subscribe((response: any) => {
      result = response.map((item: any) => item.id);
    });
    expect(result).toEqual(expected);

  });

  it('login', () => {

    let result: boolean;
    service.login('ana.young', '123').subscribe(response => result = response);
    expect(result).toBeTruthy();

  });

  it('login - invalid', () => {

    let result: any;
    service.login('invalid user', '123').subscribe({
      error: (err) => result = err,
    });

    expect(result instanceof Error).toBeTruthy();

  });

  it('logout', () => {

    const router = TestBed.get(Router);
    spyOn(router, 'navigate');

    service.logout();

    expect(router.navigate).toHaveBeenCalled();
  });

});
