import { TestBed } from '@angular/core/testing';
import { TestingModule } from '../testing/testing.module';
import { OmdbService } from './omdb.service';
import { HttpTestingController } from '@angular/common/http/testing';

describe('OmdbService', () => {

  let service: OmdbService;
  let backend: HttpTestingController;

  const URL_PARAMS = '?apikey=1f365c2b&type=movie';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
      ],
    });

    service = TestBed.get(OmdbService);
    backend = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('get', () => {

    service.get('33').subscribe();
    const request = backend.expectOne({ method: 'GET' });
    const expected = request.request.urlWithParams.endsWith(`${URL_PARAMS}&i=33`);
    expect(expected).toBeTruthy();
  });

  it('search', () => {

    service.search('star wars').subscribe();
    const request = backend.expectOne({ method: 'GET' });
    const expected = request.request.urlWithParams.endsWith(`${URL_PARAMS}&s=star%20wars`);
    expect(expected).toBeTruthy();
  });

  it('build', () => {

    const options = {name: 'stars wars', page: 2, year: 2018, plot: 'short'};
    service.build(options).subscribe();
    const request = backend.expectOne({ method: 'GET' });
    const expected = request.request.urlWithParams.endsWith(`${URL_PARAMS}&s=stars%20wars&page=2&y=2018&plot=short`);
    expect(expected).toBeTruthy();

  });

  it('build - cache', () => {

    service.build({ id: '99' }).subscribe();
    const request = backend.expectOne({ method: 'GET' });
    request.flush({});

    service.build({ id: '99' }).subscribe();
    service.build({ id: '99' }).subscribe();
    service.build({ id: '99' }).subscribe();
    service.build({ id: '99' }).subscribe();

    backend.expectNone({ method: 'GET' });

  });

});
