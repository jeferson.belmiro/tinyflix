import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { first, map } from 'rxjs/operators';
import { State, StateService } from './state.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly state: StateService,
    private readonly router: Router,
  ) { }

  get() {
    return this.state.get().pipe(
      first(),
      map((state: State) => state.user),
    );
  }

  getById(id: number) {
    return this.state.get().pipe(
      first(),
      map((state: State) => state.users.find(user => user.id === id)),
    );
  }

  getTopWatchers() {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const users = state.users;
        users.sort((a, b) => {
          return b.movies.length - a.movies.length;
        });
        return users.slice(0, 5);
      }),
    );
  }

  login(username: string, password: string) {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const found = state.users.find((user => {
          return user.username === username && user.password === password;
        }));
        if (!found) {
          throw new Error('Invalid username or password');
        }
        this.state.setValue('user', found);
        this.state.persist();
        return found;
      }),
    );
  }

  logout() {
    this.state.setValue('user', null);
    this.state.persist();
    this.router.navigate(['/auth']);
  }

}
