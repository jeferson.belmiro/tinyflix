import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { State, StateService } from './state.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(
    private readonly state: StateService,
    private readonly router: Router,
  ) {}

  canActivate(): Observable<boolean> {
    return this.state.get().pipe(
      first(),
      map((state: State) => {
        const valid = !!state.user;
        if (!valid) {
          this.router.navigate(['/auth']);
        }
        return valid;
      }),
    );
  }

  canActivateChild(): Observable<boolean> {
    return this.canActivate();
  }

}
