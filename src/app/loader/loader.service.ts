import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  static ID_DEFAULT = 'global';
  private readonly subjects = {};
  private readonly subjectsApi = {};
  private readonly data = {};

  constructor() {
    this.register(LoaderService.ID_DEFAULT);
  }

  public register(id: string) {
    if (!this.subjects[id]) {
      this.subjects[id] = new Subject<boolean>();
      this.subjectsApi[id] = this.subjects[id].asObservable();
      this.data[id] = 0;
    }
  }

  public display(id: string) {
    const data = this.data[id] > 0;
    this.subjects[id].next(data);
  }

  public show(id: string = LoaderService.ID_DEFAULT) {
    this.register(id);
    this.data[id]++;
    this.display(id);
  }

  public hide(id: string = LoaderService.ID_DEFAULT, force = false) {

    this.register(id);

    if (this.data[id] > 0) {
      this.data[id]--;
    }

    if (force) {
      this.data[id] = 0;
    }

    this.display(id);
  }

  public hideAll(force = false) {
    Object.keys(this.subjects).forEach(key => {
      this.hide(key, force);
    });
  }

  public getSubject(id: string = LoaderService.ID_DEFAULT): Observable<boolean> {
    this.register(id);
    return this.subjectsApi[id];
  }

}
