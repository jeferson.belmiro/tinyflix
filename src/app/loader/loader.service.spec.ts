import { TestBed } from '@angular/core/testing';
import { LoaderService } from './loader.service';

describe('LoaderService', () => {

  let service: LoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(LoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should hide only after all calls to hide', () => {

    const results = [];
    service.getSubject().subscribe(display => {
      results.push(display);
    });
    service.show();
    service.show();
    service.hide();
    service.hide();

    expect(results).toEqual([true, true, true, false]);

  });

  it('should hide with force', () => {

    const results = [];
    service.getSubject().subscribe(display => {
      results.push(display);
    });
    service.show();
    service.show();
    service.hide(LoaderService.ID_DEFAULT, true);
    service.hide();

    expect(results).toEqual([true, true, false, false]);

  });

  it('should display with diff ID', () => {

    const results1 = [];
    const results2 = [];

    service.getSubject('id-1').subscribe(display => {
      results1.push(display);
    });

    service.getSubject('id-2').subscribe(display => {
      results2.push(display);
    });

    service.show('id-1');
    service.show('id-2');
    service.hide('id-2');
    service.hide('id-2');

    expect(results1).toEqual([true]);
    expect(results2).toEqual([true, false, false]);

  });

  it('should hide all diff ids', () => {

    const results1 = [];
    const results2 = [];

    service.getSubject('id-1').subscribe(display => {
      results1.push(display);
    });

    service.getSubject('id-2').subscribe(display => {
      results2.push(display);
    });

    service.show('id-1');
    service.show('id-2');
    service.show('id-1');
    service.show('id-2');

    service.hideAll(true);

    expect(results1).toEqual([true, true, false]);
    expect(results2).toEqual([true, true, false]);

  });

  it('should hide all same id', () => {

    const results = [];

    service.getSubject('id-1').subscribe(display => {
      results.push(display);
    });

    service.show('id-1');
    service.show('id-1');
    service.show('id-1');
    service.show('id-1');
    service.show('id-1');
    service.show('id-1');

    service.hideAll();

    expect(results).toEqual([true, true, true, true, true, true, true]);
  });

});
