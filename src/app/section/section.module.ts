import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatRippleModule } from '@angular/material/core';
import { ListComponent } from './list/list.component';
import { MovieComponent } from './movie/movie.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    ListComponent,
    MovieComponent,
    UserComponent,
  ],
  imports: [
    CommonModule,
    MatRippleModule
  ],
  exports: [
    ListComponent,
    MovieComponent,
    UserComponent,
  ],
})
export class SectionModule { }
