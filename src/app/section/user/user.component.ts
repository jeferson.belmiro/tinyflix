import { Component, Input, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  // @TODO - add interface
  @Input()
  user: any;

  @HostBinding('class.you')
  get isYout() {
    return this.userLogged;
  }

  userLogged: boolean;


  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.get().subscribe(
      (user: any) => {
        this.userLogged = user.id === this.user.id;
      }
    );
  }

  open(user: any) {
    this.router.navigate(['/profile', user.id]);
  }

}
