import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../../shared/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  @Input()
  movie: Movie;

  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  open(movie: any) {
    this.router.navigate(['/play', movie.imdbID]);
  }

}
