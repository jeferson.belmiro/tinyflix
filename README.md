# Tinyflix

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Gitlab page
[tinyflix](http://jeferson.belmiro.gitlab.io/tinyflix)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### TODO
 - [ ] create interfaces
 - [ ] doc the code
 - [ ] more unit tests
 - [ ] e2e tests
 - [ ] responsive(basic)

### TODO[2]
 - [ ] e2e with puppeteer
 - [ ] jest
 - [ ] change homemade state service to `@ngrx/store`, use `effects` to read/write storage state

### Tips
- ZERO backend code
- All data in memory(embedded dbs like sqlite or browser storage)
- Hardcode password
- Unit test and integration test(e2e)
- Login with different users and post content.
- Component Login
- Component Main Movie Selection
- Component Play Movies
- Component Metrics
- Component Profile Page 
- 1 controller using LODASH and functional programing
- docker-compose
- doc de logic code(english only)

### Plus
- GraphQl
- Microfrontend (MOSAIC if posible)
- jest
