FROM node:12-alpine as build

WORKDIR /app
COPY . .
RUN npm ci && npm run build

FROM nginx:alpine

WORKDIR /var/www/html/public/

COPY --from=build /app/dist/tinyflix /var/www/html/public
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443
